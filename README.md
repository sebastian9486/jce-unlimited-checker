[link-jce-oracle]: https://blogs.oracle.com/java-platform-group/entry/diagnosing_tls_ssl_and_https

The Java Cryptography Extension (JCE) is an officially released Standard Extension to the Java Platform. JCE provides a framework and implementation for encryption, key generation and key agreement, and Message Authentication Code (MAC) algorithms and supplements the Java platform.

The project checks the existance/status of Java Cryptography Extension (JCE) Unlimited Strength within the JRE. 

Without the unlimited strength policy files this results in 128, after they have been installed properly the result is 2147483647. [Download JCE][link-jce-oracle] from Oracle Website.

## run.sh
```
#!/bin/sh
/path/to/jdk1.6.0_29/bin/java -classpath jce-unlimited-checker.jar com.dkv.jce.UnlimitedChecker
```