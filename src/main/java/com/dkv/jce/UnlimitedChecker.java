package com.dkv.jce;

import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;

/**
 * Without the unlimited strength policy files this results in 128, after they have been installed properly the result is 2147483647.
 * 
 * @see https://jsosic.wordpress.com/2014/02/01/check-if-jce-is-installed/
 * @see http://stackoverflow.com/questions/11538746/check-for-jce-unlimited-strength-jurisdiction-policy-files
 * @author ssommer
 */
public class UnlimitedChecker {

	public static void main(String[] args) {
		System.out.println("java.home = " + System.getProperty("java.home"));
		System.out.println("java.version = " + System.getProperty("java.version"));

		try {
			int strength = Cipher.getMaxAllowedKeyLength("AES");

			boolean supported = (strength > 128);
			System.out.println("jce unlimited support = " + supported);
			System.out.println("  >>  strength = " + strength);
		} catch (NoSuchAlgorithmException e) {
			System.out.println("jce unlimited support = false");
			e.printStackTrace();
		}
	}

}
